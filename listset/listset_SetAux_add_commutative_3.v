(* This file is generated by Why3's Coq driver *)
(* Beware! Only edit allowed sections below    *)
Require Import BuiltIn.
Require BuiltIn.
Require int.Int.

Axiom set : forall (a:Type), Type.
Parameter set_WhyType : forall (a:Type) {a_WT:WhyType a}, WhyType (set a).
Existing Instance set_WhyType.

Parameter mem: forall {a:Type} {a_WT:WhyType a}, a -> (set a) -> Prop.

(* Why3 assumption *)
Definition infix_eqeq {a:Type} {a_WT:WhyType a} (s1:(set a)) (s2:(set
  a)): Prop := forall (x:a), (mem x s1) <-> (mem x s2).

Axiom extensionality : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (infix_eqeq s1 s2) -> (s1 = s2).

(* Why3 assumption *)
Definition subset {a:Type} {a_WT:WhyType a} (s1:(set a)) (s2:(set
  a)): Prop := forall (x:a), (mem x s1) -> (mem x s2).

Axiom subset_refl : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  (subset s s).

Axiom subset_trans : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)) (s3:(set a)), (subset s1 s2) -> ((subset s2 s3) -> (subset s1
  s3)).

Parameter empty: forall {a:Type} {a_WT:WhyType a}, (set a).

(* Why3 assumption *)
Definition is_empty {a:Type} {a_WT:WhyType a} (s:(set a)): Prop :=
  forall (x:a), ~ (mem x s).

Axiom empty_def1 : forall {a:Type} {a_WT:WhyType a}, (is_empty (empty : (set
  a))).

Axiom mem_empty : forall {a:Type} {a_WT:WhyType a}, forall (x:a), ~ (mem x
  (empty : (set a))).

Parameter add: forall {a:Type} {a_WT:WhyType a}, a -> (set a) -> (set a).

Axiom add_def1 : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (y:a),
  forall (s:(set a)), (mem x (add y s)) <-> ((x = y) \/ (mem x s)).

Parameter remove: forall {a:Type} {a_WT:WhyType a}, a -> (set a) -> (set a).

Axiom remove_def1 : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (y:a)
  (s:(set a)), (mem x (remove y s)) <-> ((~ (x = y)) /\ (mem x s)).

Axiom add_remove : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set
  a)), (mem x s) -> ((add x (remove x s)) = s).

Axiom remove_add : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set
  a)), ((remove x (add x s)) = (remove x s)).

Axiom subset_remove : forall {a:Type} {a_WT:WhyType a}, forall (x:a) (s:(set
  a)), (subset (remove x s) s).

Parameter union: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) -> (set
  a).

Axiom union_def1 : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)) (x:a), (mem x (union s1 s2)) <-> ((mem x s1) \/ (mem x s2)).

Parameter inter: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) -> (set
  a).

Axiom inter_def1 : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)) (x:a), (mem x (inter s1 s2)) <-> ((mem x s1) /\ (mem x s2)).

Parameter diff: forall {a:Type} {a_WT:WhyType a}, (set a) -> (set a) -> (set
  a).

Axiom diff_def1 : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)) (x:a), (mem x (diff s1 s2)) <-> ((mem x s1) /\ ~ (mem x s2)).

Axiom subset_diff : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (subset (diff s1 s2) s1).

Parameter choose: forall {a:Type} {a_WT:WhyType a}, (set a) -> a.

Axiom choose_def : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  (~ (is_empty s)) -> (mem (choose s) s).

Parameter cardinal: forall {a:Type} {a_WT:WhyType a}, (set a) -> Z.

Axiom cardinal_nonneg : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  (0%Z <= (cardinal s))%Z.

Axiom cardinal_empty : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  ((cardinal s) = 0%Z) <-> (is_empty s).

Axiom cardinal_add : forall {a:Type} {a_WT:WhyType a}, forall (x:a),
  forall (s:(set a)), (~ (mem x s)) -> ((cardinal (add x
  s)) = (1%Z + (cardinal s))%Z).

Axiom cardinal_remove : forall {a:Type} {a_WT:WhyType a}, forall (x:a),
  forall (s:(set a)), (mem x s) -> ((cardinal s) = (1%Z + (cardinal (remove x
  s)))%Z).

Axiom cardinal_subset : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (subset s1 s2) -> ((cardinal s1) <= (cardinal s2))%Z.

Axiom subset_eq : forall {a:Type} {a_WT:WhyType a}, forall (s1:(set a))
  (s2:(set a)), (subset s1 s2) -> (((cardinal s1) = (cardinal s2)) ->
  (infix_eqeq s1 s2)).

Axiom cardinal1 : forall {a:Type} {a_WT:WhyType a}, forall (s:(set a)),
  ((cardinal s) = 1%Z) -> forall (x:a), (mem x s) -> (x = (choose s)).

Axiom remove_invariance : forall {a:Type} {a_WT:WhyType a}, forall (e:a)
  (s1:(set a)), (~ (mem e s1)) -> (s1 = (remove e (add e s1))).

Axiom add_1 : forall {a:Type} {a_WT:WhyType a}, forall (e:a) (e1:a) (s:(set
  a)) (s1:(set a)), (s1 = (add e s)) -> ((mem e1 s1) -> ((mem e1 s) \/
  (e = e1))).

Axiom add_2 : forall {a:Type} {a_WT:WhyType a}, forall (e:a) (s:(set a))
  (s1:(set a)), (s1 = (add e s)) -> (mem e s1).

Axiom add_3 : forall {a:Type} {a_WT:WhyType a}, forall (e:a) (s:(set a))
  (s1:(set a)), (s1 = (add e s)) -> ((mem e s1) /\ forall (e':a), (mem e'
  s) -> (mem e' s1)).

Theorem add_commutative : forall {a:Type} {a_WT:WhyType a}, forall (e1:a)
  (e2:a) (s:(set a)) (s1:(set a)) (s2:(set a)), (~ (e1 = e2)) ->
  ((s1 = (add e1 (add e2 s))) -> ((s2 = (add e2 (add e1 s))) -> (s1 = s2))).
intros a a_WT e1 e2 s s1 s2 h1 h2 h3.
apply extensionality.
red.
intro.
split;intros.
pose proof add_1 e1 x (add e2 s) s1 h2 H.
destruct H0.
rewrite h3.
apply add_def1.
apply add_def1 in H0.
destruct H0.
auto.
right.
apply add_def1.
auto.
rewrite h2 in H.
(*2 : rewrite h2 .

2 : assumption.
*)
rewrite h3.
rewrite H0.
generalize H.
intro.

apply add_def1.
right.
apply add_def1.
left.
reflexivity.
(* goal 1 done *)
(*
pose proof add_1 e2 x (add e1 s) s2 h3 H.
destruct H0.
rewrite h2.
apply add_def1.
apply add_def1 in H0.
destruct H0.
auto.
right.
apply add_def1.
auto.
rewrite h3 in H.
rewrite h2.
apply add_def1.
apply add_def1 in H.
right.
rewrite H0.
apply add_def1.
auto.
*)



(*
add_1 : (e:a) (e1:a) (s:(set a)) (s1:(set a)), (s1 = (add e s)) -> ((mem e1 s1) 
  -> ((mem e1 s) \/ (e = e1))).
1 subgoal
a : Type
a_WT : WhyType a
e1, e2 : a
s, s1, s2 : set a
h1 : e1 <> e2
h2 : s1 = add e1 (add e2 s)
h3 : s2 = add e2 (add e1 s)
x : a
H : (mem e1 s1) mem x s2, e1 =x, s1 = s2
______________________________________(1/1)
mem x (add e1 (add e2 s))
*)

pose proof add_1 e2 x (add e1 s) s2 h3 H.
destruct H0.
rewrite h2.
apply add_def1.
apply add_def1 in H0.
destruct H0.
left.
auto.
right.
apply add_def1.
right.
auto.

rewrite h3 in H.
rewrite h2.


apply add_def1.
right.
rewrite H0.
apply add_def1.
auto.

Qed.

(* Why3 goal *)
Theorem add_commutative : forall {a:Type} {a_WT:WhyType a}, forall (e1:a)
  (e2:a) (s:(set a)) (s1:(set a)) (s2:(set a)), (s1 = (add e1 (add e2 s))) ->
  ((s2 = (add e2 (add e1 s))) -> (s1 = s2)).
(* Why3 intros a a_WT e1 e2 s s1 s2 h1 h2. *)
intros a a_WT e1 e2 s s1 s2 h1 h2.

generalize h1.
intro h1_.
apply add_3 in h1.
destruct h1 as (h1_0, h1_1).
pose proof (@add_4 _ _ _ _ _ _ h1_0 _).

eapply add_4 in h1_1.

generalize H0.
intro.



generalize h2.
intro.
apply add_3 in h2.
destruct h2.


Qed.

