CoreCert

Certified and extractable RTFM-core compiler
Per Lindgren 2016 (c)

Extraction 
why3 -L . extract -D ocaml64 CoreCertExt.mlw -o extract 

Type instation, for the demonstration
cp coretypes__CoreTypes.ml_patch coretypes__CoreTypes.ml

Build 
ocamlbuild -r main.native -use-ocamlfind -package why3 -package why3.extract



