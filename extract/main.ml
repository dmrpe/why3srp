open Why3extract
open Why3extract.Why3__BigInt
open Listmap__Map
open Coreast__CoreAST
open CoreCertExt__Comp 
open CoreCertExt__CompImp 
open CoreCertExt__TaskRes
open CoreCertExt__TaskResImp
open Format

let nl = "\n"
let to_str x = to_string x

let rec pt = function
 | []           -> ""
 | Op c :: l    -> c ^ "; " ^ pt l
 | Lock r :: l  -> "L " ^ r ^ "; " ^ pt l             
 | Unlock r ::l -> "U " ^ r ^ "; " ^ pt l

let main () =
 let i1 = Claim ("R1", O "o=z.x-z.y" :: []) :: [] in
 let i2 = O "v--" ::
          Claim ("R2",
                  Claim ("R1", O "z.x=z.y=v" :: []) :: []
          ) :: [] in
 let i3 = Claim ("R2", O "v++" :: []) :: [] in 
 let t1 = { prio = of_int 2; prog = comp_imp i1 } in
 let t2 = { prio = of_int 1; prog = comp_imp i2 } in
 let t3 = { prio = of_int 3; prog = comp_imp i3 } in
 let ts = t1 :: t2 :: t3 :: [] in

 begin match (safe_rc_of_tasks ts) with
 | Some rc ->
   printf "Flattened task set:%sT1 [%s]%sT2 [%s]%sT3 [%s]%s"
    nl (pt t1.prog) nl (pt t2.prog) nl (pt t3.prog) nl ;
   printf "Resource ceilings : r1 = %s, r2 = %s%s"
    (to_str (get rc ("R1"))) (to_str (get rc ("R2"))) nl;
 | None   -> printf "Compilation failed.%s" nl;
 end;;
main ();;
